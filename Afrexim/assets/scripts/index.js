/*global login, handleLiveChange, document, window, alert*/
"use strict";

// TODO: Send & Parse HTTP Requests & responses
// TODO: publish the app to AWS

function handleListLiveChange(val) {
    const pattern = new RegExp("[^A-z\\s-]");
    if (pattern.test(val.value) && val.value.length !== 0) {
        val.value = "";
        snackBar("Only alphabets");
        val.style.borderColor = "red";
    } else {
        val.style.borderColor = "springgreen";
        val.setAttribute("value", val.value);

    }
}

function handleLiveChange(val) {
    var emailElement = document.getElementById("form-username");
    const pattern =
        new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if (val.length > 0 && pattern.test(val)) {
        emailElement.style.borderColor = "springgreen";
    } else {
        emailElement.style.borderColor = "red";
    }
}

function validateEmail(email) {
    const emailPattern = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return email.length > 0 && emailPattern.test(email);
}

function login() {
    var email = document.getElementById("form-username").value,
        password = document.getElementById("form-password").value;
    if (!validateEmail(email) && password.length === 0)
        return;
    var url = "", params = "email=" + email + "&password=" + password;
    var postResponse = sendHttpRequest("POST", url, params),
        postResponseMessage = postResponse.responseText.data.Message.value;
    if (postResponseMessage === 1) {
        $(myCarousel).carousel(1);
        setTimeout(alert("If this  is the first time, please upload csv file of your HODs, do the mapping afterwards. \n After that enjoy your app"), 1500);
    } else {
        redirectError();
    }
}

function redirectError() {
    window.location = "assets/errorPage/error.html";
}

function createTableFromJSON(jsonObj) {
    document.getElementById("dataMap").style.visibility = "hidden";
    // for demo
    var jsonObj = {
        "success": true,
        "data": [
            {
                "ScorecardGuid": 13,
                "DepartmentName": "TRFI- Trade Finance Department"
            },
            {
                "ScorecardGuid": 259,
                "DepartmentName": "ADMI Scorecard"
            },
            {
                "ScorecardGuid": 264,
                "DepartmentName": "INAU Scorecard"
            },
            {
                "ScorecardGuid": 269,
                "DepartmentName": "BAOP Scorecard"
            },
            {
                "ScorecardGuid": 274,
                "DepartmentName": "BOSE Scorecard"
            },
            {
                "ScorecardGuid": 284,
                "DepartmentName": "COMM Scorecard"
            },
            {
                "ScorecardGuid": 289,
                "DepartmentName": "COMP Scorecard"
            },
            {
                "ScorecardGuid": 294,
                "DepartmentName": "CRAS Scorecard"
            },
            {
                "ScorecardGuid": 299,
                "DepartmentName": "FINA Scorecard"
            },
            {
                "ScorecardGuid": 304,
                "DepartmentName": "HURE Scorecard"
            },
            {
                "ScorecardGuid": 309,
                "DepartmentName": "INTE Scorecard"
            },
            {
                "ScorecardGuid": 314,
                "DepartmentName": "LEGA Scorecard"
            },
            {
                "ScorecardGuid": 319,
                "DepartmentName": "RICO Scorecard"
            },
            {
                "ScorecardGuid": 324,
                "DepartmentName": "RIMA Scorecard"
            },
            {
                "ScorecardGuid": 329,
                "DepartmentName": "STIN Scorecard"
            },
            {
                "ScorecardGuid": 339,
                "DepartmentName": "TREA Scorecard"
            },
            {
                "ScorecardGuid": 1611,
                "DepartmentName": "ACMA Scorecard"
            },
            {
                "ScorecardGuid": 1944,
                "DepartmentName": "IATI Scorecard"
            },
            {
                "ScorecardGuid": 2266,
                "DepartmentName": "SYAG Scorecard"
            },
            {
                "ScorecardGuid": 2309,
                "DepartmentName": "GUSF Scorecard"
            },
            {
                "ScorecardGuid": 2351,
                "DepartmentName": "EXDE Scorecard"
            },
            {
                "ScorecardGuid": 2394,
                "DepartmentName": "PRFI Scorecard"
            },
            {
                "ScorecardGuid": 2434,
                "DepartmentName": "CLRE Scorecard"
            },
            {
                "ScorecardGuid": 3893,
                "DepartmentName": "CQAS Scorecard"
            },
            {
                "ScorecardGuid": 4068,
                "DepartmentName": "EQMO Scorecard"
            }
        ]
    };
    // EXTRACT VALUE FOR HTML HEADER.
    var col = [];
    for (var j = 0; j < jsonObj.data.length; j++) {
        for (var key in jsonObj.data[j]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    col.push("SAP ERP");
    col.push("Head Of Department Name");
    col.push("Head Of Department Code");

    // CREATE DYNAMIC TABLE, WITH "table" CLASS & ID
    var table = document.createElement("table");
    table.className = "table";
    table.id = "table";

    var getHODS = sendHttpRequest("GET", "", "");

    // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
    var tr = table.insertRow(-1);                   // TABLE ROW.
    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        tr.appendChild(th);
    }

    // ADD JSON DATA TO THE TABLE AS ROWS.
    for (var i = 0; i < jsonObj.data.length; i++) {

        tr = table.insertRow(-1);

        var inputCell = document.createElement("td"), labelCell = document.createElement("td"),
            dropdownCell = document.createElement("td");

        var select = document.createElement("select");
        select.setAttribute("onchange", "updateList(this)");
        select.className = "form-control btn";

        var option = new Option("Choose HOD by code", "value #00", true, true), i;
        select.appendChild(option);
        for (i = 0; i < getHODS.length; i++) {
            var option = new Option(getHODS.name, getHODS.name, false, false);
            select.appendChild(option);
        }
        dropdownCell.appendChild(select);

        var label = document.createElement("label");
        label.innerHTML = "N/A";
        labelCell.appendChild(label);

        var input = document.createElement("input");
        input.className = "mappingInput";
        input.setAttribute("type", "text");
        input.setAttribute("onclick", "snackBar('Please enter alphabets only.')");
        input.setAttribute("onkeypress", "handleListLiveChange(this)");
        input.setAttribute("onchange", "handleListLiveChange(this)");
        input.setAttribute("value", "");
        inputCell.appendChild(input);


        for (var j = 0; j < col.length; j++) {
            if (jsonObj.data[i][col[j]] == undefined)
                continue;
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = jsonObj.data[i][col[j]];
            tr.appendChild(inputCell);
            tr.appendChild(labelCell);
            tr.appendChild(dropdownCell);
        }

    }

    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    var divContainer = document.getElementById("showData");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);
    document.getElementById("dataShow").disabled = true;
    document.getElementById("dataShow").style.visibility = "hidden";
}

function updateList(argsThis) {
    var oTable = document.getElementById('table'), rowClickedIndex = val.parentNode.parentNode.rowIndex,
        selectedValue = val.value, tdsInRow = oTable.rows.item(rowClickedIndex).getElementsByTagName("td"),
        cellLabels = tdsInRow[3].getElementsByTagName("label");
    cellLabels[0].innerHTML = selectedValue;
}

function mapDataSFQS() {
    $(myCarousel).carousel(2);
}

function back() {
    $(myCarousel).carousel(1);
}

function snackBar(message) {
    var snackBar = document.getElementById("snackbar");
    snackBar.className = "show";
    snackBar.innerHTML = message;

    setTimeout(function () {
        snackBar.className = snackBar.className.replace("show", "");
    }, 3000);
}

function sendHttpRequest(xhrMethod, url, params) {
    var xhr = new XMLHttpRequest();
    var isAsync = true;
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            return xhr;
        }
        redirectError();
    };
    xhr.open(xhrMethod, url, isAsync);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.withCredentials = true;
    xhr.send(params);
}

function handleFiles(files) {
    // Check for the various File API support.
    if (window.FileReader) {
        // FileReader are supported.
        getAsText(files[0]);
    } else {
        alert('FileReader are not supported in this browser.');
    }
}

function getAsText(fileToRead) {
    var reader = new FileReader();
    // Handle errors load
    reader.onload = loadHandler;
    reader.onerror = errorHandler;
    // Read file into memory as UTF-8
    reader.readAsText(fileToRead);
}

function loadHandler(event) {
    var csv = event.target.result;
    csvJSON(csv);
}

function isEmpty(obj) {
    return (Object.keys(obj).length === 0 && obj.constructor === Object);
}

function csvJSON(csv) {
    var lines = csv.split("\n"), result = [], headers = lines[0].split(",");
    for (var i = 1; i < lines.length; i++) {
        var obj = {}, currentline = lines[i].split(",");
        for (var j = 0; j < headers.length; j++) {
            if (currentline[j] === "" || currentline[j] === undefined)
                continue;
            obj[headers[j]] = currentline[j];
        }
        if (isEmpty(obj)) continue;
        result.push(obj);
    }
    //return result; //JavaScript object
    // return JSON.stringify(result); //JSON
    // alert(result[0]["User Name"]);
    // var url = "", params = "HODs" + JSON.stringify(result), postResponse = sendHttpRequest("POST", url, params);
    alert(JSON.stringify(result));
}

function errorHandler(evt) {
    if (evt.target.error.name == "NotReadableError") {
        alert("Can't read file!");
    }
}

function test() {
    $(myCarousel).carousel(1);
    snackBar("Testing a process.");
}

function dim() {
    var dimmer = $('.dimmer');
    dimmer.show();
}

function undim() {
    var dimmer = $('.dimmer');
    dimmer.hide();
}

function processing() {
    snackBar("Processing your request...");
    dim();
    $('#loading').show();
    setTimeout(undim, 3000);
}

function mapData() {
    var oTable = document.getElementById('table');
    if (oTable == null) {
        snackBar("You have to click 'Show Data' first");
        return;
    }
    var rowsLength = oTable.rows.length;
    var mapJSONArr = [];
    var mapJSONObj = {};

    for (var i = 0; i < rowsLength; i++) {
        var inputs = oTable.rows.item(i).getElementsByTagName("input");
        var inputsLength = inputs.length;
        for (var j = 0; j < inputslength; j++) {
            var inputval = inputs[j].value;
            if (inputval != "") {
                mapJSONObj["DepartmentToMap"] = oTable.rows.item(i).cells.item(1).innerHTML.toString();
                mapJSONObj["MappingValue"] = inputval;
                mapJSONArr.push(mapJSONObj);
            }
        }
    }
    var params = "dataMapping=" + mapJSONArr, url = "", postResponse = sendHttpRequest("POST", url, params);
}

function syncQSandSF() {
    var url = "", params = "", postResponse = sendHttpRequest("POST", url, params);
}

function syncSFandQS() {
    var url = "", params = "", postResponse = sendHttpRequest("POST", url, params);
}

function logOut() {
    var url = "", params = "", postResponse = sendHttpRequest("POST", url, params);
}

function showData() {
    // var url = "", params = "", getResponse = sendHttpRequest("GET", url, params);
    createTableFromJSON("jsonObj");
}

// function processData(csv) {
//     var allTextLines = csv.split(/\r\n|\n/);
//     var lines = [];
//     while (allTextLines.length) {
//         lines.push(allTextLines.shift().split(','));
//     }
//     alert(lines);
//     // sendHttpRequest("POST", "", lines);
//     drawOutput(lines);
// }

// function drawOutput(lines){
//     //Clear previous data
//     document.getElementById("output").innerHTML = "";
//     var table = document.createElement("table");
//     for (var i = 0; i < lines.length; i++) {
//         var row = table.insertRow(-1);
//         for (var j = 0; j < lines[i].length; j++) {
//             var firstNameCell = row.insertCell(-1);
//             firstNameCell.appendChild(document.createTextNode(lines[i][j]));
//         }
//     }
//     document.getElementById("output").appendChild(table);
// }

// $(window).ready(function() {
//     $('#loading').hide();
// });

// dropdownCell.className = "dropdown";
// select.className = "btn btn-default actionButton";
// select.id = "myDropdown";
// select.innerHTML = "Choose HOD by Code ▼";
// select.setAttribute("data-toggle", "dropdown");
// select.setAttribute("href", "#");
// select.setAttribute("onclick", "showDropDown(this)");