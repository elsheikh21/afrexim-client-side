# Afrexim Bank #
## _client side_ ##

### Summary ###
* Version 1.0.0
* [App](https://bitbucket.org/tutorials/markdowndemo)
* **Developer:** Ahmed ElSheikh
* Functionality: 
    * Login form 
    * Dashboard contains 4 Jumbtrons
        * csv file 
            * parse and send to backend.
        * Mapping data 
            * drop down menu and input fields.
        * Upload data to SF
        * Upload data to QS 
    * Back buttons and logout